﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using WMPLib;

namespace AlienPlainShooter
{
    public partial class Form1 : Form
    {
        // Definition for Sound ---------------------------------------------------------------
        WindowsMediaPlayer gameBackgroundMedia;
        WindowsMediaPlayer shootBombMedia;
        WindowsMediaPlayer explosion;

        // Definition for moving background ------------------------------------------------
        PictureBox[] stars;
        int bgSpeed;
        int playerSpeed;

        // Definition for Shooted bomb --------------------------------------------------------------------------------------------------
        PictureBox[] playerbombs;
        int playerBombSpeed;

        // Definition for Shooted bomb --------------------------------------------------------------------------------------------------
        PictureBox[] enemiesbombs;
        int enemiesBombSpeed;

        // Definition for Enemies -------------------------------------------------------------------------------------------------------
        PictureBox[] enemies;
        int enemiSpeed;

        Random random;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // Background Setting (Implementation) --------------------------------------------------------------------------------------
            bgSpeed = 4;
            playerSpeed = 4;
            enemiSpeed = 4;
            playerBombSpeed = 20;
            enemiesBombSpeed = 4;

            // Shooting Bomb with Player -------------------------------------------------------------------------------------------------
            playerbombs = new PictureBox[3];
            Image bomb = Image.FromFile(@"E:\Projects\Desktop Projects\AlienPlainShooter\AlienPlainShooter\asserts\munition.png");
            for(int i=0;i<playerbombs.Length;i++)
            {
                playerbombs[i] = new PictureBox();
                playerbombs[i].Size = new Size(8, 8);
                playerbombs[i].Image = bomb;
                playerbombs[i].SizeMode = PictureBoxSizeMode.Zoom;
                playerbombs[i].BorderStyle = BorderStyle.None;
                this.Controls.Add(playerbombs[i]);
            }

            // Loading Enemies Image -----------------------------------------------------------------------------------------------------
            enemies = new PictureBox[10];
            for(int i=0;i<enemies.Length;i++)
            {
                enemies[i] = new PictureBox();
                enemies[i].Size = new Size(40, 40);
                enemies[i].SizeMode = PictureBoxSizeMode.Zoom;
                enemies[i].BorderStyle = BorderStyle.None;
                enemies[i].Visible = false;
                this.Controls.Add(enemies[i]);
                enemies[i].Location = new Point((i + 1) * 50, -50);
            }

            Image enemy1 = Image.FromFile(@"E:\Projects\Desktop Projects\AlienPlainShooter\AlienPlainShooter\asserts\E1.png");
            Image enemy2 = Image.FromFile(@"E:\Projects\Desktop Projects\AlienPlainShooter\AlienPlainShooter\asserts\E2.png");
            Image enemy3 = Image.FromFile(@"E:\Projects\Desktop Projects\AlienPlainShooter\AlienPlainShooter\asserts\E3.png");
            Image bossEnemy1 = Image.FromFile(@"E:\Projects\Desktop Projects\AlienPlainShooter\AlienPlainShooter\asserts\Boss1.png");
            Image bossEnemy2 = Image.FromFile(@"E:\Projects\Desktop Projects\AlienPlainShooter\AlienPlainShooter\asserts\Boss2.png");

            enemies[0].Image = bossEnemy1;
            enemies[1].Image = enemy1;
            enemies[2].Image = enemy2;
            enemies[3].Image = enemy3;
            enemies[4].Image = enemy1;
            enemies[5].Image = enemy2;
            enemies[6].Image = enemy3;
            enemies[7].Image = enemy1;
            enemies[8].Image = enemy2;
            enemies[9].Image = bossEnemy2;

            // Sound Media section (Implementation) --------------------------------------------------------------------------------------
            gameBackgroundMedia = new WindowsMediaPlayer();
            shootBombMedia = new WindowsMediaPlayer();
            explosion = new WindowsMediaPlayer();

            gameBackgroundMedia.URL = "E:\\Projects\\Desktop Projects\\AlienPlainShooter\\AlienPlainShooter\\songs\\GameSong.mp3";
            shootBombMedia.URL = "E:\\Projects\\Desktop Projects\\AlienPlainShooter\\AlienPlainShooter\\songs\\shoot.mp3";
            explosion.URL = "E:\\Projects\\Desktop Projects\\AlienPlainShooter\\AlienPlainShooter\\songs\\boom.mp3";

            gameBackgroundMedia.settings.setMode("loop", true);
            gameBackgroundMedia.settings.volume = 5;
            shootBombMedia.settings.volume = 3;
            explosion.settings.volume = 7;

            // Game Background(space with stars) -------------------------------------------------------------------------------------------
            stars = new PictureBox[15];
            random = new Random();

            for(int i=0;i<stars.Length;i++)
            {
                stars[i] = new PictureBox();
                stars[i].BorderStyle = BorderStyle.None;
                stars[i].Location = new Point(random.Next(20, 580), random.Next(-10, 400));
                if(i % 2 == 1)
                {
                    stars[i].Size = new Size(2, 2);
                    stars[i].BackColor = Color.Wheat;
                }
                else
                {
                    stars[i].Size = new Size(4, 4);
                    stars[i].BackColor = Color.DarkGray;
                }
                this.Controls.Add(stars[i]);
            }

            // Enemies BombFiring -------------------------------------------------------------------------------------------
            enemiesbombs = new PictureBox[10];

            for(int i=0;i<enemiesbombs.Length;i++)
            {
                enemiesbombs[i] = new PictureBox();
                enemiesbombs[i].Size = new Size(2, 25);
                enemiesbombs[i].Visible = false;
                enemiesbombs[i].BackColor = Color.Yellow;
                int x = random.Next(0, 10);
                enemiesbombs[i].Location = new Point(enemies[x].Location.X, enemies[x].Location.Y - 20);
                this.Controls.Add(enemiesbombs[i]);
            }

            gameBackgroundMedia.controls.play();
        }

        // Timer Section --------------------------------------------------------------------------------------------------------------------
        private void MoveBgTimer_Tick(object sender, EventArgs e)
        {
            for(int i=0;i<stars.Length/2;i++)
            {
                stars[i].Top += bgSpeed;
                if(stars[i].Top>=this.Height)
                {
                    stars[i].Top = -stars[i].Height;
                }
            }
            for(int i=stars.Length/2;i<stars.Length;i++)
            {
                stars[i].Top += bgSpeed - 2;
                if(stars[i].Top >= this.Height)
                {
                    stars[i].Top = -stars[i].Height;
                }
            }
        }

        private void LeftMoveTimer_Tick(object sender, EventArgs e)
        {
            if(Player.Left>10)
            {
                Player.Left -= playerSpeed;
            }
        }

        private void RightMoveTimer_Tick(object sender, EventArgs e)
        {
            if (Player.Right < 580)
            {
                Player.Left += playerSpeed;
            }
        }

        private void UpMoveTimer_Tick(object sender, EventArgs e)
        {
            if (Player.Top >10)
            {
                Player.Top -= playerSpeed;
            }
        }

        private void DownMoveTimer_Tick(object sender, EventArgs e)
        {
            if (Player.Top < 400)
            {
                Player.Top += playerSpeed;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Right)
            {
                RightMoveTimer.Start();
            }
            if (e.KeyCode == Keys.Left)
            {
                LeftMoveTimer.Start();
            }
            if (e.KeyCode == Keys.Up)
            {
                UpMoveTimer.Start();
            }
            if (e.KeyCode == Keys.Down)
            {
                DownMoveTimer.Start();
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            RightMoveTimer.Stop();
            LeftMoveTimer.Stop();
            UpMoveTimer.Stop();
            DownMoveTimer.Stop();
        }

        private void MovePlayerBombTimer_Tick(object sender, EventArgs e)
        {
            shootBombMedia.controls.play();
            for(int i=0;i<playerbombs.Length;i++)
            {
                if(playerbombs[i].Top>0)
                {
                    playerbombs[i].Visible = true;
                    playerbombs[i].Top -= playerBombSpeed;

                    collision();
                }
                else
                {
                    playerbombs[i].Visible = false;
                    playerbombs[i].Location = new Point(Player.Location.X + 20, Player.Location.Y - i * 30);
                }
            }
        }

        private void MoveEnemiesTimer_Tick(object sender, EventArgs e)
        {
            moveEnemies(enemies, enemiSpeed);
        }

        // Method to move enemy and their actions(enemies fire)
        private void moveEnemies(PictureBox[] array,int speed)
        {
            for(int i=0; i<array.Length;i++)
            {
                array[i].Visible = true;
                array[i].Top += speed;
                if(array[i].Top > this.Height)
                {
                    array[i].Location = new Point((1 + 0) * 50, -200);
                }
            }
        }

        // method to collision
        private void collision()
        {
            for(int i=0;i<enemies.Length;i++)
            {
                if(playerbombs[0].Bounds.IntersectsWith(enemies[i].Bounds)
                    || playerbombs[1].Bounds.IntersectsWith(enemies[i].Bounds)
                    || playerbombs[2].Bounds.IntersectsWith(enemies[i].Bounds))
                {
                    explosion.controls.play();
                    enemies[i].Location = new Point((i + 1) * 50, -100);
                }
                if(Player.Bounds.IntersectsWith(enemies[i].Bounds))
                {
                    explosion.settings.volume = 30;
                    explosion.controls.play();
                    Player.Visible = false;
                    gameOver("Game Over");
                }
            }
        }

        // method to Game Over
        private void gameOver(string str)
        {
            gameBackgroundMedia.controls.stop();
            stopTimer();
        }

        private void stopTimer()
        {
            MoveBgTimer.Stop();
            MoveEnemiesTimer.Stop();
            MovePlayerBombTimer.Stop();
            EnemiesBombTimer.Stop();
        }

        private void startTimer()
        {
            MoveBgTimer.Start();
            MoveEnemiesTimer.Start();
            MovePlayerBombTimer.Start();
            EnemiesBombTimer.Start();
        }

        private void EnemiesBombTimer_Tick(object sender, EventArgs e)
        {
            for(int i=0;i<enemiesbombs.Length;i++)
            {
                if(enemiesbombs[i].Top < this.Height)
                {
                    enemiesbombs[i].Visible = true;
                    enemiesbombs[i].Top += enemiesBombSpeed;
                    collisionWithEnemiesBomb();
                }
                else
                {
                    enemiesbombs[i].Visible = false;
                    int x = random.Next(0, 10);
                    enemiesbombs[i].Location = new Point(enemies[x].Location.X + 20, enemies[x].Location.X + 30);
                }
            }
        }

        // collision with enemies bombs
        private void collisionWithEnemiesBomb()
        {
            for(int i = 0;i < enemiesbombs.Length;i++)
            {
                if(enemiesbombs[i].Bounds.IntersectsWith(Player.Bounds))
                {
                    enemiesbombs[i].Visible = false;
                    explosion.settings.volume = 30;
                    explosion.controls.play();
                    Player.Visible = false;
                    gameOver("Game Over");
                }
            }
        }
    }
}
